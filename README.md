# gitlab-docker

Repo to host your Gitlab infrastructure using docker-compose on your machine.

## Getting Started

After cloning the repo configure a dns entry for http://gitlab-web in the /etc/hosts or on your dns server.
Execute the command:

docker-compose up

the above command takes some minutes to be up and running.

When it is done you must create an admin account (root) and create a new password.

To connect the runner simply run the script register_runner.sh with the correct registration_token.

All of the steps described above have to be run only once.

## Gitlab config

now the default server is puma but if you want to use unicorn uncomment the lines with the '#' sign and remove the puma['worker_processes'].

      GITLAB_OMNIBUS_CONFIG: |
        external_url 'http://gitlab-web'
        gitlab_rails['gitlab_shell_ssh_port'] = 10022

## Gitlab Docker Registry

to be able to connect to the Gitlab Docker Registry the docker client needs to have this configured as insecure if no ssl certificate is provided.

In the `daemon.json` file of docker add those lines

```
{
  "insecure-registries" : ["gitlab-web:5050"]
}

```

## Troubleshooting

In macOS maybe there is a bug after a while. Increasing the ulimit -a -n: file descriptor to a value like 524288 could fix the problem. Check the documentation online on how to change it with the current macOS version.
